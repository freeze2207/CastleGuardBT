using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[TaskCategory("Guard")]
[TaskDescription("Returns Success if still has spot to check")]
public class hasCheckSpot : Conditional
{
    public SharedTransformList checkList;
    public override TaskStatus OnUpdate()
    {
        return checkList.Value.Count > 0 ? TaskStatus.Success : TaskStatus.Failure;
    }
}
