using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BehaviorTree))]
public class CastleGuardController : MonoBehaviour
{
    [SerializeField] private float damageSpeed = 2.0f;
    private BehaviorTree btTree;
    private SharedFloat health;

    // Start is called before the first frame update
    void Start()
    {
        btTree = GetComponent<BehaviorTree>();

        health = btTree.GetVariable("Health") as SharedFloat;
    }

    private void OnTriggerStay(Collider other)
    {
        health.Value -= Time.deltaTime * damageSpeed;
    }
}
