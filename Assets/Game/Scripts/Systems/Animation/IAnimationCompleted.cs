﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationCompleted
{
	void animationCompleted(int shortHashName);
}
