using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[TaskCategory("Guard")]
[TaskDescription("Set next checking location")]
public class SetNextLocation : Action
{
    public SharedTransformList checkList;
    public SharedGameObject targetGameObject;
    public SharedTransform nextLocation;

    // cache the navmeshagent component
    private NavMeshAgent navMeshAgent;
    private GameObject prevGameObject;

    public override void OnStart()
    {
        var currentGameObject = GetDefaultGameObject(targetGameObject.Value);
        if (currentGameObject != prevGameObject)
        {
            navMeshAgent = currentGameObject.GetComponent<NavMeshAgent>();
            prevGameObject = currentGameObject;
        }
    }

    public override TaskStatus OnUpdate()
    {
        if (navMeshAgent == null)
        {
            Debug.LogWarning("NavMeshAgent is null");
            return TaskStatus.Failure;
        }
        nextLocation.Value = GetNextSpot();
        if (nextLocation.Value != null)
        {
            navMeshAgent.SetDestination(nextLocation.Value.position);
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }

    public override void OnReset()
    {
        targetGameObject = null;
        nextLocation = null;
    }

    private Transform GetNextSpot()
    {
        int index = 0;
        float nearestDistance = Vector3.Distance(targetGameObject.Value.transform.position, checkList.Value[index].position);
        Transform spot = checkList.Value[index];
        for (int i = 0; i < checkList.Value.Count; i++)
        {
            if (Vector3.Distance(checkList.Value[i].position, targetGameObject.Value.transform.position) < nearestDistance)
            {
                index = i;
                spot = checkList.Value[i];
            }
        }

        return spot;
    }
}
