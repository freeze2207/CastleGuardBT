using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[TaskCategory("Guard")]
[TaskDescription("Add alert destination")]
public class ClickToAlert : Action
{
    public SharedTransformList checkList;
    public GameObject checkpointGO;
    public SharedFloat lastClick;
    public float interval = 0.1f;

    private float betweenTime = 0.0f;

    public override void OnStart()
    {
        base.OnStart();
        betweenTime = Time.time - lastClick.Value;
        lastClick.Value = Time.time;
    }

    public override TaskStatus OnUpdate()
    {
        if (betweenTime > interval)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (checkList.Value.Count < 5)
                {
                    GameObject go = GameObject.Instantiate(checkpointGO, hit.point, new Quaternion(0, 0, 0, 0));
                    checkList.Value.Add(go.transform);
                }

            }
        }
        
        return TaskStatus.Success;
    }


    public override void OnDrawGizmos()
    {
        /*for (int i = 0; i < spotList.Count; i++)
        {
            DebugExtension.DebugPoint(spotList[i], Color.red);
        }*/
    }
}
