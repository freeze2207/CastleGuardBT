using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[TaskCategory("Guard")]
[TaskDescription("Remove alert destination")]
public class RemoveCheckPoint : Action
{
    public SharedTransformList checkList;
    public SharedTransform nextLocation;

    public override TaskStatus OnUpdate()
    {
        if (nextLocation.Value != null)
        {
            Transform spot = nextLocation.Value;
            nextLocation.Value = null;
            GameObject.Destroy(spot.gameObject);
            return checkList.Value.Remove(spot) ? TaskStatus.Success : TaskStatus.Failure;
        }

        return TaskStatus.Running;
    }
}
