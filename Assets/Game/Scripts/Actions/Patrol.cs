using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[TaskCategory("Guard")]
[TaskDescription("NPC patrols a given set of waypoints")]
public class Patrol : MoveToGoal
{
	public SharedGameObjectList waypoints;
	private int index = 0;
	private SharedInt waypointCount;
	private SharedString idleName;

	public override void OnStart()
	{
		base.OnStart();
		waypointCount = Owner.GetVariable("waypointCount") as SharedInt;
		idleName = Owner.GetVariable("IdleName") as SharedString;
		agent.SetDestination(waypoints.Value[index].transform.position);
	}

	public override TaskStatus OnUpdate()
	{
		TaskStatus baseStatus = base.OnUpdate();
		if (baseStatus == TaskStatus.Success)
		{
			index++;
			waypointCount.Value++;
			idleName.Value = "Idle" + Random.Range(0, 3);
            
			if (index >= waypoints.Value.Count)
			{
				index = 0;
			}
			agent.SetDestination(waypoints.Value[index].transform.position);
		}

		return TaskStatus.Running;
	}
}
