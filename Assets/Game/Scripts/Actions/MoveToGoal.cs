using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[TaskCategory("Guard")]
[TaskDescription("Moves an agent to the destination")]
public class MoveToGoal : Action
{
	public float angularDampeningTime = 5.0f;
	public float deadZone = 10.0f;

	protected NavMeshAgent agent;
	protected Animator animator;

	protected AnimationListener animationListener;
	protected UnityAction onAnimatorMoveCallback;

	//private bool firstFrame;

	public override void OnAwake()
	{
		agent = Owner.gameObject.GetComponent<NavMeshAgent>();
		// Add Assets if we want?
		animator = gameObject.GetComponent<Animator>();
		// Add Assets if we want?

		animationListener = gameObject.GetComponent<AnimationListener>();
		// Add Assets if we want?

		onAnimatorMoveCallback = new UnityAction(OnAnimatorMove);
	}

	public override void OnStart()
	{
		animationListener.addAnimatorMoveListener(onAnimatorMoveCallback);
		//firstFrame = true;
	}

	private void OnAnimatorMove()
	{
		agent.velocity = animator.deltaPosition / Time.deltaTime;
	}

	public override TaskStatus OnUpdate()
	{
		if (agent.desiredVelocity != Vector3.zero)
		{
			//firstFrame = false;

			float speed = Vector3.Project(agent.desiredVelocity, transform.forward).magnitude * agent.speed;
			animator.SetFloat("speed", speed);

			float angle = Vector3.Angle(transform.forward, agent.desiredVelocity);
			if (Mathf.Abs(angle) <= deadZone)
			{
				transform.LookAt(transform.position + agent.desiredVelocity);
			}
			else
			{
				transform.rotation = Quaternion.Lerp(transform.rotation,
													 Quaternion.LookRotation(agent.desiredVelocity),
													 Time.deltaTime * angularDampeningTime);
			}
			return TaskStatus.Running;
		}

		animator.SetFloat("speed", 0.0f);
		return TaskStatus.Success;
	}

	public override void OnEnd()
	{
		animationListener.removeAnimatorMoveListener(onAnimatorMoveCallback);
	}

    public override void OnDrawGizmos()
    {
#if UNITY_EDITOR
		DebugExtension.DebugArrow(transform.position, agent.desiredVelocity, Color.red);
		DebugExtension.DebugPoint(agent.destination, Color.green);
#endif
	}
}
