using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BehaviorTree))]
public class GizmoGrawing : MonoBehaviour
{
    private BehaviorTree btTree;
    private SharedTransformList checkList;

    private void Start()
    {
        btTree = GetComponent<BehaviorTree>();

        checkList = btTree.GetVariable("CheckList") as SharedTransformList;
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        for (int i = 0; i < checkList.Value.Count; i++)
        {
            DebugExtension.DrawLocalCube(checkList.Value[i], new Vector3(1.0f, 1.0f, 1.0f), Color.red);
        }
#endif
    }
}
